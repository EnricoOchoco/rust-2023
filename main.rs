//Functions as an import
use std::io;

//Functions as an object
struct Schedule {
    room_number: u32,
    date: String,
    time: String,
}

//Functions as an interface
trait CheckAvailability {
    fn is_available(&self, room: &str, date: &str, time: &str) -> Result<bool, &'static str>;
}

//Implements the interface
impl CheckAvailability for Vec<Schedule> {
    fn is_available(&self, room: &str, date: &str, time: &str) -> Result<bool, &'static str> {
        for sched in self {
            if sched.room_number == room.parse().unwrap()
                && sched.date == date
                && sched.time == time
            {
                return Ok(false); // Room is unavailable
            }
        }
        Ok(true) // Room is available
    }
}

fn get_user_input(prompt: &str) -> String {
    println!("{}", prompt);
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line");
    input.trim().to_string()
}

fn main() {
    let schedules = vec![
        Schedule {
            room_number: 522,
            date: "2023-09-21".to_string(),
            time: "10:00 AM - 12:00 PM".to_string(),
        },
        Schedule {
            room_number: 525,
            date: "2023-09-21".to_string(),
            time: "2:00 PM - 4:00 PM".to_string(),
        },
    ];

    let schedules_ref = &schedules;
    let mut is_available = false;

    while !is_available {
        // Get user input for room number, date, and time
        let room_number = get_user_input("Enter room number:");
        let date = get_user_input("Enter date (yyyy-mm-dd):");
        let time = get_user_input("Enter time (hh:mm AM/PM - hh:mm AM/PM):");

        // Check room availability using the custom trait method
        match schedules_ref.is_available(&room_number, &date, &time) {
            Ok(true) => {
                println!(
                    "Room {} is available on {} at {}.",
                    room_number, date, time
                );
                is_available = true;
            }
            Ok(false) => {
                println!(
                    "Room {} is unavailable on {} at {}.",
                    room_number, date, time
                );
            }
            Err(error) => {
                println!("{}", error);
            }
        }
    }
}
